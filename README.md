# Instalador de docker y docker-compose para sistemas Debian o Ubuntu

## Uso

` git clone https://gitlab.com/luirro777/docker_install`

`cd docker_install`

### Debian

`chmod +x ./install_debian.sh`

`sudo ./install_debian.sh`

### Ubuntu

`chmod +x ./install_ubuntu.sh`

`sudo ./install_ubuntu.sh`

